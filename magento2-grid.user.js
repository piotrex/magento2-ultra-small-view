// ==UserScript==
// @name         Magento grid - ultra small view
// @namespace    https://gitlab.com/piotrex/
// @version      0.1.0
// @description  Magento grid - ultra small view
// @author       zerk
// @grant           unsafeWindow
// @require      https://greasyfork.org/scripts/369404-waitforkeyelementsindocument/code/waitForKeyElementsInDocument.js?version=606324
// ==/UserScript==

var css = `
.data-grid td, .data-grid th {
font-size:9px;
line-height:9px;
vertical-align:middle !important;
}
.data-grid td {
padding-top: 2px !important;
padding-bottom: 2px !important;
padding-left: 1px !important;
padding-right: 1px !important;
border-left: 0.1rem solid #959595;
border-right: 0.1rem solid rgba(156,156,156,0.57);
/*color: #303030;*/
}
.data-grid th {
overflow-wrap: break-word;
word-wrap: break-word;
-webkit-hyphens: auto;
-ms-hyphens: auto;
-moz-hyphens: auto;
hyphens: auto;
text-align:center;
padding-top:5px !important;
padding-bottom:5px !important;
}

.data-grid tr {
border-bottom: 0.1rem solid rgba(156,156,156,0.57);
}


.data-grid td .data-grid-cell-content {
text-align:left;
/*    overflow-wrap: break-word;
word-wrap: break-word;
-webkit-hyphens: auto;
-ms-hyphens: auto;
-moz-hyphens: auto;
hyphens: auto;*/
}

.data-grid td label{
text-align:center;
}

.data-grid td img:not(:hover){
height: 20px !important;
width: 20px !important;
}
.data-grid td label::before {
height:13px;
width:13px;
}
.data-grid-checkbox-cell-inner {
padding:0;
}

.menu-wrapper {

}

.admin__menu li.level-0 > a {
min-height:40px !important;
padding: 5px !important;
font-size:8px;
}

.admin__menu .level-0 > a::before {
transform: scale(0.7)
}
.menu-wrapper .logo {
transform: scale(0.7);
}
.menu-wrapper, .menu-wrapper::before {
width:60px
}

.page-wrapper {
margin-left:60px !important;
}
.data-grid-cap-left {
width:0px !important;
}
.sticky-header {
padding-left:3px;
}

.page-main-actions:not(._hidden), .page-actions._fixed {
padding: 3px;
display:none;
}
.sticky-header{
top:0px
}
.admin__data-grid-header-row * {
/*height:20px !important;
font-size:10px;
line-height: 9px; !important;
padding:3 !important;*/
}
.admin__data-grid-wrap {
padding-top:0px;
}
.action-select-wrap, .action-select-wrap button, .action-select-wrap button span {
/*height:20px !important;
font-size:10px;
line-height: 8px; !important;
padding:0 !important;*/
}
.sticky-header .admin__data-grid-header {
display:none;
}

.data-grid .data-grid-thumbnail-cell {
width: unset;
}
/*
jeszcze:
podgląd img na najechaniu
menu po lewej dostępne po najechaniu
*/
`;

// https://stackoverflow.com/a/524721/1794387
function addStyle(css, id) {
    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');
    style.type = 'text/css';
    if(id) {
        style.id = id;
    }
    if (style.styleSheet){
        // This is required for IE8 and below.
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
    head.appendChild(style);
    return style;
}

function removeStyle(style) {
    style.parentNode.removeChild(style);
}
var styleApplied = false;
var idStyle = "deGjjgfDGG";
function toogleRestyle() {
    if(styleApplied) {
        var style = document.getElementById(idStyle);
        removeStyle(style);
    } else {
        addStyle(css, idStyle);
    }
    styleApplied = !styleApplied;
}


//var selector = ".admin__data-grid-header-row";
var selector = "header";
function doAction() {
        var container = document.querySelector(selector);
    var div = document.createElement("a");
    div.href="javascript:void(0);";
    div.innerHTML = "TOOGLE";
    div.addEventListener("click", function(ev) {
        toogleRestyle();
        return false;
    });
    container.insertBefore(div, container.firstChild);
}
var waiting =5000;
setTimeout(doAction, waiting);
//waitForKeyElementsInDocument(selector, doAction,false);
